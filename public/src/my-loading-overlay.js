import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "@polymer/paper-spinner/paper-spinner.js";


class MyLoadingOverlay extends PolymerElement {

  static get template() {
    return html`
      <style>

        .overlay {
          position: absolute;
          z-index: 100; /* make higher than whatever is on the page */
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background: #FFF;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
        }
        /* https://github.com/Polymer/polymer/issues/3711#issuecomment-226361758 */
        .overlay[hidden] {
          display: none !important;
        }
        .overlay paper-spinner {
          width: 50px;
          height: 50px;
        }

      </style>
  
      <!-- Loading page https://www.youtube.com/watch?v=iAgSvlYavX0 -->
      <div class="overlay">
          <paper-spinner active></paper-spinner>
      </div>
    `;
  }

}

customElements.define('my-loading-overlay', MyLoadingOverlay);
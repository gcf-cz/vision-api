import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";

import "@polymer/app-media/app-media-devices.js";
import "@polymer/app-media/app-media-stream.js";
import "@polymer/app-media/app-media-video.js";
import "@polymer/app-media/app-media-image-capture.js";

import "@polymer/iron-image/iron-image.js";
import "@polymer/paper-toast/paper-toast.js";


class MyImageCapture extends PolymerElement {

  static get template() {
    return html`
      <!--
      https://www.polymer-project.org/blog/2017-10-18-upcoming-changes.html
      https://github.com/Polymer/polymer/issues/4679
      -->
      <style>

        .paper-card-container {
          margin: 16px;
          @apply --layout-horizontal;
          @apply --layout-center-justified;
        }
        paper-card {
          --paper-card: {
            min-width: 300px;
            max-width: 800px;
          }
        }
        app-media-video {
          max-height: 400px;
        }
        img {
          margin-top: 24px;
          width: 40%;
        }

      </style>

      <!-- The computer is connected to devices... -->
      <app-media-devices
        id="mediaDevices"
        kind="videoinput"
        selected-device="{{videoDevice}}"
        devices="{{videoDevice}}">
      </app-media-devices>

      <!-- https://github.com/PolymerElements/app-media/issues/23 -->
      <app-media-stream
        video-constraints={{videoConstraints}}
        stream="{{videoStream}}"
        active>
      </app-media-stream>

      <app-media-image-capture
        id="imageCapture"
        stream="[[videoStream]]"
        photo-capabilities={{photoCapabilities}}
        focus-mode="single-shot"
        red-eye-reduction
        last-photo="{{photo}}">
      </app-media-image-capture>

      <div class="paper-card-container">
        <paper-card>
          <div class="card-content">

          <app-media-video
            id="video"
            source="[[videoStream]]"
            on-click="_takePhoto"
            autoplay
            muted
            contain>
          </app-media-video>

          <img id="snapshot" src$="[[imageUrl]]" hidden>

          </div>
          <div class="card-actions">
            <paper-button raised on-tap="_takePhoto">Take a photo</paper-button>
          </div>
        </paper-card>
      </div>

      <paper-toast id="toast" text="[[message]]"></paper-toast>
    `;
  }

  static get properties() {
    return {
      data: Object,
      videoDevice: Object,
      devices: Array,
      videoStream: Object,
      photo: Object,
      imageUrl: String,
      photoCapabilities: Object,
      //https://developer.mozilla.org/en-US/docs/Web/API/MediaStreamTrack/applyConstraints
      //https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
      videoConstraints: {
        type: Object,
        value: function() {
          return {
            facingMode: "environment",
            width: { min: 640, ideal: 1280, max: 1920 },
            height: { min: 480, ideal: 720, max: 1080 }
          };
        }
      }
    };
  }
  static get observers() {
    return [
      '_photoChanged(photo)'
    ];
  }

  async _takePhoto() {
    try {
      console.log(this.photoCapabilities);
      console.log(navigator.mediaDevices.getSupportedConstraints());
      let blob = await this.$.imageCapture.takePhoto();
      let storageRef = firebase.storage().ref();
      let fileName = new Date().toISOString().replace("T","_").replace("Z","")+'_photo.jpg';
      let snapshot = await storageRef.child(fileName).put(blob);
      console.log('The photo has been successfully uploaded.');
      this.$.toast.show('The photo has been successfully uploaded.');
    } catch(error) {
      console.error(error.toString());
      this.$.toast.show(error.toString());
    };
  }

  _photoChanged(blob) {
    let imageUrl = URL.createObjectURL(blob);
    this.imageUrl = imageUrl;
    //https://developers.google.com/web/updates/2016/12/imagecapture#capture
    let img = this.$.snapshot;
    img.hidden = false;
    img.onload = function() { URL.revokeObjectURL(this.src); }
  }

}

customElements.define('my-image-capture', MyImageCapture);
import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import "@polymer/app-layout/app-header-layout/app-header-layout.js";
import "@polymer/app-layout/app-header/app-header.js";
import "@polymer/app-layout/app-toolbar/app-toolbar.js";

import "@polymer/paper-icon-button/paper-icon-button.js";
import "@polymer/iron-icon/iron-icon.js";
import "@polymer/iron-icons/iron-icons.js";

import "./my-loading-overlay.js";
import "./my-image-capture.js";


class MyApp extends PolymerElement {

  static get template() {
    return html`
      <style>

        app-toolbar {
          background-color: #4285f4;
          color: #fff;
        }
        iron-icon + [main-title] {
          margin-left: 24px;
        }
        iron-icon {
          padding: 8px;
        }
        @media (max-width: 430px) {
          .hide-email {
            display: none;
          }
        }

      </style>

      <my-loading-overlay hidden$="[[user]]"></my-loading-overlay>
      
      <app-header-layout fullbleed has-scrolling-region>
        <app-header slot="header">
          <app-toolbar>
            <iron-icon icon="perm-media"></iron-icon>
            <div main-title>Face Detection</div>
            <span class="hide-email" style="font-size: 15px; padding-right: 15px;" hidden$="[[!user]]">[[user.email]]</span>
            <paper-icon-button icon="power-settings-new" on-tap="_logout" hidden$="[[!user]]"></paper-icon-button>
          </app-toolbar>
        </app-header>
        
        <my-image-capture></my-image-capture>

      </app-header-layout>
    `;
  }

  static get properties() {
    return {
      user: Object
    };
  }

  //https://www.polymer-project.org/2.0/docs/about_20#lifecycle-changes
  //https://www.polymer-project.org/2.0/docs/devguide/registering-elements#lifecycle-callbacks
  connectedCallback() {
    super.connectedCallback();

    this.login();
  }

  async login() {
    let THIS = this;
    firebase.auth().onAuthStateChanged(user => {
      if (!user) {
        //https://github.com/firebase/polymerfire/issues/276#issuecomment-334402506
        //https://developers.google.com/identity/protocols/OpenIDConnect#authenticationuriparameters
        let provider = new firebase.auth.GoogleAuthProvider();
        provider.setCustomParameters({
          prompt: 'select_account'
        });
        firebase.auth().signInWithRedirect(provider);
      } else {
        THIS.user = user;
      }
    });
  }

  _logout() {
    firebase.auth().signOut();
  }

}

customElements.define('my-app', MyApp);
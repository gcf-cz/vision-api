"use strict";

require('@google-cloud/debug-agent').start({ allowExpressions: true });

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const VISION = require('@google-cloud/vision');
const vision = new VISION.ImageAnnotatorClient();

//https://github.com/googleapis/nodejs-bigquery/releases/tag/v2.0.0
const {BigQuery} = require('@google-cloud/bigquery');
const bigquery = new BigQuery();

exports.processImage = functions.storage.object().onFinalize(async (object) => {

  let results = await annotateImage(object.bucket, object.name);
  console.log(results);

  await faceAnnotations(results[0].faceAnnotations, object.bucket, object.name);
  await labelAnnotations(results[0].labelAnnotations, object.bucket, object.name);
  
});


/*
https://cloud.google.com/vision/docs/detecting-faces
https://github.com/googleapis/nodejs-vision/blob/master/samples/detect.js
https://github.com/firebase/functions-samples/blob/master/bigquery-import/functions/index.js
*/
async function faceAnnotations(annotations, bucket_name, file_name) {
  try {
    if(!annotations || annotations.length === 0) return;
    console.log("faceAnnotations");

    let bq_row = {
      img_url: `gs://${bucket_name}/${file_name}`,
      img_name: file_name,
      a: []
    }

    for (let i = 0; i < annotations.length; i++) {
      let annotation = annotations[i];
      console.log(annotation);
      
      bq_row.a.push({
        detectionConfidence: annotation.detectionConfidence,
        landmarkingConfidence: annotation.landmarkingConfidence,
        joy: annotation.joyLikelihood,
        sorrow: annotation.sorrowLikelihood,
        anger: annotation.angerLikelihood,
        surprise: annotation.surpriseLikelihood,
        underExposed: annotation.underExposedLikelihood,
        blurred: annotation.blurredLikelihood,
        headwear: annotation.headwearLikelihood
      });
    }

    console.log(bq_row);
    let table = bigquery.dataset("visionapi").table("faceAnnotations");
    await table.insert(bq_row);

  } catch(error) {
    console.error(error.toString());
  }
};


async function labelAnnotations(annotations, bucket_name, file_name) {
  try {
    if(!annotations || annotations.length === 0) return;
    console.log("labelAnnotations");

    let bq_row = {
      img_url: `gs://${bucket_name}/${file_name}`,
      img_name: file_name,
      a: []
    }

    for (let i = 0; i < annotations.length; i++) { 
      let annotation = annotations[i];
      console.log(annotation);

      bq_row.a.push({
        locale: annotation.locale,
        description: annotation.description,
        score: annotation.score,
        topicality: annotation.topicality
      });
    }

    console.log(bq_row);
    let table = bigquery.dataset("visionapi").table("labelAnnotations");
    await table.insert(bq_row);

  } catch(error) {
    console.error(error.toString());
  }
};


async function annotateImage(bucketName, fileName) {
  try {
    console.log(`Analyzing the image ${fileName}`);
    let results = await vision.annotateImage({
      image: {
        source: {
          imageUri:
            `gs://${bucketName}/${fileName}`
        }
      },
      features: [
        {
          type: "FACE_DETECTION",
          maxResults: 3
        },
        {
          type: "LABEL_DETECTION",
          maxResults: 3
        },
        {
          type: "LANDMARK_DETECTION",
          maxResults: 3
        },
        {
          type: "TEXT_DETECTION"
        }
      ]
    });

    return results;

  } catch(error) {
    console.error(error.toString());
  }
};
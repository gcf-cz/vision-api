CREATE TEMP FUNCTION convertLikelihood(state STRING)
  RETURNS FLOAT64
  LANGUAGE js AS """
  
      switch (state) {
        case 'VERY_UNLIKELY':
          state = -2;
          break;
        case 'UNLIKELY':
          state = -1;
          break;
        case 'POSSIBLE':
          state = 0;
          break;
        case 'LIKELY':
          state = 1;
          break;
        case 'VERY_LIKELY':
          state = 2;
          break;
        default:
          state = 99;
      }
      return state;

""";

WITH table AS (
  SELECT
    img_name,
    convertLikelihood(annotation.joy) AS joy,
    convertLikelihood(annotation.sorrow) AS sorrow,
    convertLikelihood(annotation.anger) AS anger,
    convertLikelihood(annotation.surprise) AS surprise,
    convertLikelihood(annotation.underExposed) AS underExposed,
    convertLikelihood(annotation.blurred) AS blurred,
    convertLikelihood(annotation.headwear) AS headwear
  FROM `fb-cloud-functions-cz.visionapi.faceAnnotations`,
    UNNEST (a) AS annotation
)

SELECT
  COUNT(IF( joy > 0 AND joy < 99, TRUE, NULL )) AS joy,
  COUNT(IF( sorrow > 0 AND sorrow < 99, TRUE, NULL )) AS sorrow,
  COUNT(IF( anger > 0 AND anger < 99, TRUE, NULL )) AS anger,
  COUNT(IF( surprise > 0 AND surprise < 99, TRUE, NULL )) AS surprise,
  COUNT(IF( underExposed > 0 AND underExposed < 99, TRUE, NULL )) AS underExposed,
  COUNT(IF( blurred > 0 AND blurred < 99, TRUE, NULL )) AS blurred,
  COUNT(IF( headwear > 0 AND headwear < 99, TRUE, NULL )) AS headwear
FROM table